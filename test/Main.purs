module Test.Main where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpec)

import Test.Lang.Calc.Main as Calc
import Test.Lang.CoreLanguage.Main as CoreLanguage
import Test.Lang.UntypedLambda.Main as UntypedLambda


main :: Effect Unit
main = launchAff_ $ runSpec [consoleReporter] do
  Calc.main
  CoreLanguage.main
  UntypedLambda.main
