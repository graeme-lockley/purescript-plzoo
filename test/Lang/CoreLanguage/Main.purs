module Test.Lang.CoreLanguage.Main
  ( main
  ) where

import Prelude (class Monad, class Show, Unit, bind, discard, negate, show, ($), (<>))

import Control.Monad.Error.Class (class MonadThrow)
import Data.Either (Either(..), hush)
import Data.List ((:), List(..))
import Data.Maybe (Maybe(..))
import Text.Parsing.Parser (runParser)

import Effect.Exception (Error)
import Test.Spec (SpecT, describe, it)
import Test.Spec.Assertions (shouldEqual)

import Lang.CoreLanguage.Lexer (variable)
import Lang.CoreLanguage.Machine (Node(..), State(..))
import Lang.CoreLanguage.Machine.Heap as Heap
import Lang.CoreLanguage.Main as Main
import Lang.CoreLanguage.Parser (parse)


main :: forall a b. Monad b => MonadThrow Error a => SpecT a Unit b Unit
main =
  let
    validateVariable value =
      it value do
        runParser value variable `shouldEqual` Right value

    parseShow :: forall c d. Show c => Show d => Either c d -> String
    parseShow (Left x) = "Left " <> show x
    parseShow (Right x) = show x

    validateParser input output =
      it input do
        (parseShow $ parse input) `shouldEqual` output

    runResult' :: String -> Maybe Int
    runResult' input = do
      x <- hush $ Main.run' input
      runResult x

    runResult :: List State -> Maybe Int
    runResult Nil = Nothing
    runResult ((State s) : Nil) =
      case s.stack of
        Nil -> Nothing
        (v : Nil) ->
          case Heap.lookup v s.heap of
            Just (NNum n) -> Just n
            _ -> Nothing
        _ -> Nothing
    runResult (_ : ss) = runResult ss

    runMachine input result =
      it input do
        runResult' input `shouldEqual` Just result
  in
    describe "Core Language" do
      describe "Lexer" do
        describe "variable" do
          validateVariable "Hello"
          validateVariable "a123"
          validateVariable "a1_23"
          validateVariable "_123"

      describe "Parser" do
        validateParser
          "ID x = x"
          "ID x =\n  x"

        validateParser
          "plus1 n = n + 1"
          "plus1 n =\n  + n 1"

        validateParser
          "calc a b = a * 3 + b * 2"
          "calc a b =\n  + (* a 3) (* b 2)"

        validateParser
          "cons x xs = Pack{1, 2} x xs ; nil = Pack{0, 0}"
          "cons x xs =\n  Pack{1, 2} x xs ;\n\nnil =\n  Pack{0, 0}"

        validateParser
          "depth t = case t of <1> n -> 0 ; <2> t1 t2 -> 1 + max (depth t1) (depth t2)"
          "depth t =\n  case t of\n    <1>n -> 0 ;\n    <2>t1 t2 -> + 1 (max (depth t1) (depth t2))"

        validateParser
          "f x = let y = x*x in y*y"
          "f x =\n  let\n    y =\n      * x x\n  in\n    * y y"

        validateParser
          "f x = letrec y = x*x in y*y"
          "f x =\n  letrec\n    y =\n      * x x\n  in\n    * y y"

        validateParser
          "main = let double = \\x. x * x in double 10"
          "main =\n  let\n    double =\n      \\x. * x x\n  in\n    double 10"

        it "small standard prelude" do
          (parseShow $ parse smallStandardPreludeText) `shouldEqual` smallStandardPreludeText

      describe "Machine" do
        runMachine "main = 42" 42
        runMachine "I x = x ; main = I (I 42)" 42
        runMachine "I x = x ; main = (I I) 42" 42
        runMachine "K x y = x ; S f g x = f x (g x) ; main = S K K 3" 3
        runMachine "I x = x ; main = let ii = I I ; x = ii 47 ; y = ii x in ii y" 47
        runMachine "K x y = x ; K1 x y = y ; pair x y f = f x y ; fst p = p K ; snd p = p K1 ; f x y = letrec a = pair x b ; b = pair y a in fst (snd (snd (snd a))) ; main = f 3 4" 4
        runMachine "main = negate 10" (-10)
        runMachine "I x = x ; main = negate (negate (negate (I I 10)))" (-10)
        runMachine "main = (6 + 2 * 3) / 4 - 2" 1

smallStandardPreludeText :: String
smallStandardPreludeText =
  "I x =\n  x ;\n\n" <>
  "K x y =\n  x ;\n\n" <>
  "K1 x y =\n  y ;\n\n" <>
  "S f g x =\n  f x (g x) ;\n\n" <>
  "compose f g x =\n  f (g x) ;\n\n" <>
  "twice f =\n  compose f f"
