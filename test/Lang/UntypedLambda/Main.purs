module Test.Lang.UntypedLambda.Main
  ( main
  ) where

import Prelude (class Monad, Unit, discard, show, ($), (<<<), (<>))

import Control.Monad.Error.Class (class MonadThrow)
import Control.Monad.State (evalState)
import Data.Either (Either(..))
import Data.Map (empty, insert)
import Text.Parsing.Parser (runParser)

import Effect.Exception (Error)
import Test.Spec (SpecT, describe, it)
import Test.Spec.Assertions (shouldEqual)

import Lang.UntypedLambda.Eval (Environment, InterpreterState(..), ReductionStrategy(..), reduce, substitute)
import Lang.UntypedLambda.Lexer (constant)
import Lang.UntypedLambda.Parser (parse, parseExpression)
import Lang.UntypedLambda.Syntax (Expression(..))


main :: forall t37 t38. Monad t38 => MonadThrow Error t37 => SpecT t37 Unit t38 Unit
main =
  describe "Untyped Lambda" do
    describe "Lexer" do
      describe "constant" do
        it "a" do
          runParser "a" constant `shouldEqual` Right "a"

        it "123" do
          runParser "123" constant `shouldEqual` Right "123"

        it "+" do
          runParser "+" constant `shouldEqual` Right "+"

    describe "Syntax" do
      describe "show" do
        it "Variable \"true\"" do
          (show $ Variable "true") `shouldEqual` "true"

        it "Lambda \"t\" (Lambda \"f\" (Variable \"t\"))" do
          (show $ Lambda "t" $ Lambda "f" $ Variable "t") `shouldEqual` "(\\t. (\\f. t))"

        it "Lambda \"s\" (Lambda \"z\" (Apply (Variable \"s\") (Apply (Variable \"s\") (Apply (Variable \"s\") (Variable \"z\"))" do
          (show $ Lambda "s" $ Lambda "z" $ Apply (Variable "s") (Apply (Variable "s") (Apply (Variable "s") (Variable "z")))) `shouldEqual` "(\\s. (\\z. s (s (s z))))"

        it "Apply (Lambda \"x\" (Variable \"x\")) (Lambda \"x\" (Variable \"x\"))" do
          (show $ Apply (Lambda "x" $ (Apply (Variable "x") (Variable "x") ) ) (Lambda "x" $ (Apply (Variable "x")  (Variable "x")))) `shouldEqual` "(\\x. x x) (\\x. x x)"

    describe "Parser" do
      describe "expression" do
        it "\\x. x x" do
          (show $ parse "\\x. x x") `shouldEqual` "(Right ((\\x. x x) : Nil))"

        it "(\\x. x x) 10" do
          (show $ parse "(\\x. x x) 10") `shouldEqual` "(Right ((\\x. x x) 10 : Nil))"

        it "(\\x. x x) (\\x. x x)" do
          (show $ parse "(\\x. x x) (\\x. x x)") `shouldEqual` "(Right ((\\x. x x) (\\x. x x) : Nil))"

        it "\\f. \\s. \\b. b f s" do
          (show $ parse "\\f. \\s. \\b. b f s") `shouldEqual` "(Right ((\\f. (\\s. (\\b. (b f) s))) : Nil))"

      describe "assign" do
        it "infinity := (\\x. x x) (\\x. x x)" do
          (show $ parse "infinity := (\\x. x x) (\\x. x x)") `shouldEqual` "(Right (infinity := (\\x. x x) (\\x. x x) : Nil))"

        it "2 := \\f. \\s. \\b. b f s" do
          (show $ parse "2 := \\f. \\s. \\b. b f s") `shouldEqual` "(Right (2 := (\\f. (\\s. (\\b. (b f) s))) : Nil))"

    describe "Eval" do
      describe "reduce" do
        it "(\\x. x) 10" do
          reduceString "(\\x. x) 10" `shouldEqual` Variable "10"

        it "HEAD (CONS p q)" do
          (show $ reduceString "HEAD (CONS p q)") `shouldEqual` "p"

        it "TAIL (CONS p q)" do
          (show $ reduceString "TAIL (CONS p q)") `shouldEqual` "q"

      describe "substitute" do
        it "(\\z. x) with x -> z" do
          (show $ substitute "x" (Variable "z") (Lambda "z" (Variable "x"))) `shouldEqual` "(\\z'. z)"


defaultEnv :: Environment
defaultEnv =
  insert "CONS" (parseE empty "\\a. (\\b. (\\f. (f a) b))") $
  insert "HEAD" (parseE empty "\\c. c (\\a. \\b. a)") $
  insert "TAIL" (parseE empty "\\c. c (\\a. \\b. b)") empty


reduceString :: String -> Expression
reduceString =
  (oreduce defaultEnv) <<< (parseE defaultEnv)


oreduce :: Environment -> Expression -> Expression
oreduce env value =
  evalState (reduce value) $ InterpreterState { environment: env, strategy: WeakHeadNormalForm }


parseE :: Environment -> String -> Expression
parseE env t =
  case runParser t parseExpression of
    Left x -> Variable ("parse error: " <> show x)
    Right r -> r