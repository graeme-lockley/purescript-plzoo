module Test.Lang.Calc.Main
  ( main
  ) where

import Prelude (class Monad, Unit, discard, show, ($))

import Control.Monad.Error.Class (class MonadThrow)
import Data.Either (Either(..))
import Effect.Exception (Error)
import Text.Parsing.Parser (ParseError, runParser)
import Test.Spec (SpecT, describe, it)
import Test.Spec.Assertions (shouldEqual)

import Lang.Calc.Parser (parseOpExpression)
import Lang.Calc.Syntax (Expression(..))


main :: forall t37 t38. Monad t38 => MonadThrow Error t37 => SpecT t37 Unit t38 Unit
main =
  describe "Calc" do
    describe "Expression" do
      describe "Syntax" do
        describe "show" do
          it "Num 0" do
            (show $ Num 0) `shouldEqual` "0"
          it "Negate (Num 10)" do
            (show $ Negate (Num 10)) `shouldEqual` "-10"
          it "Plus (Num 0) (Num 1)" do
            (show $ Plus (Num 0) (Num 1)) `shouldEqual` "0 + 1"
          it "Multiply (Plus (Num 0) (Num 1)) (Num 2)" do
            (show $ Multiply (Plus (Num 0) (Num 1)) (Num 2)) `shouldEqual` "(0 + 1) * 2"

      describe "Parser" do
        describe "constant" do
            it "0" do
              parse "0" `shouldEqual` Right (Num 0)
            it "100a" do
              parse "100a" `shouldEqual` Right (Num 100)

        describe "binary operator" do
          it "10 + 3" do
            parse "10 + 3" `shouldEqual` Right (Plus (Num 10) (Num 3))
          it "10 - 3 * 5" do
            parse "10 - 3 * 5" `shouldEqual` Right (Minus (Num 10) (Multiply (Num 3) (Num 5)))
          it "1 + 2 + 3 - 4 + 5" do
            parse "1 + 2 + 3 - 4 + 5" `shouldEqual` Right (Plus (Minus (Plus (Plus (Num 1) (Num 2)) (Num 3)) (Num 4)) (Num 5))

        describe "parenthesis" do
          it "-(10 + 3)" do
            parse "-(10 + 3)" `shouldEqual` Right (Negate (Plus (Num 10) (Num 3)))
          it "(10 - 3) * 5" do
            parse "(10 - 3) * 5" `shouldEqual` Right (Multiply (Minus (Num 10) (Num 3)) (Num 5))
          it "(1 + 1) * (2 + 2)" do
            parse "(1 + 1) * (2 + 2)" `shouldEqual` Right  (Multiply (Plus (Num 1) (Num 1)) (Plus (Num 2) (Num 2)))


parse :: String -> Either ParseError Expression
parse text =
  runParser text parseOpExpression
