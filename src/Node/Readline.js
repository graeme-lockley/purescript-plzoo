/* global exports */
'use strict';

// module Node.Readline

const Action = require("../Node.Readline.Action/index");


exports.createInterfaceImpl = function () {
    const readline = require('readline');

    return readline.createInterface(
        {
            input: process.stdin,
            output: process.stdout,
            historySize: 100,
            prompt: '> '
        });
};


exports.prompt = function (readline) {
    return function () {
        readline.prompt();
    };
};


exports.setHandler = function (readline) {
    return function (initial) {
        return function (lineHandler) {
            return function () {
                let current =
                    initial;

                readline.removeAllListeners('line');
                readline.on('line', function (line) {
                    const result =
                        lineHandler(line)(current)();

                    // console.log(`** Result.fst: ${JSON.stringify(result.value0, null, 2)}`);
                    // console.log(`** Result.snd: ${JSON.stringify(result.value1, null, 2)}`);

                    current = result.value1;

                    if (result.value0 instanceof Action.Close)
                        readline.close();
                    if (result.value0 instanceof Action.Prompt)
                        readline.prompt();
                });
            };
        };
    };
};


exports.setPrompt = function (prompt) {
    return function(readline) {
        return function() {
          readline.setPrompt(prompt);
        };
    };
};