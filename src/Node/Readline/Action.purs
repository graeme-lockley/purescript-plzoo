module Node.Readline.Action
  ( Action(..)
  ) where


data Action
  = Prompt
  | Close
