module Node.Readline
  ( Interface
  , LineHandler
  , createInterface
  , prompt
  , setHandler
  , setPrompt
  ) where

import Prelude

import Effect (Effect)

import Data.Tuple (Tuple)

import Node.Readline.Action (Action)


foreign import data Interface :: Type

foreign import createInterfaceImpl :: Effect Interface


createInterface :: Effect Interface
createInterface =
  createInterfaceImpl


foreign import prompt :: Interface -> Effect Unit


type LineHandler a =
  String -> a -> Effect (Tuple Action a)


foreign import setHandler :: forall a. Interface -> a -> LineHandler a -> Effect Unit

foreign import setPrompt :: String -> Interface -> Effect Unit


