module Main where

import Prelude (($), (==), (<>), Unit, bind, discard, pure, unit)

import Data.Array ((!!), find, intercalate)
import Data.Functor (map)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (log)
import Node.Process (argv)

import Lang.Calc.Main as Calc
import Lang.CoreLanguage.Main as Core
import Lang.UntypedLambda.Main as UntypedLambda

import Tools.PLZoo.Main as Main


main :: Effect Unit
main = do
  args <- argv
  process args


process :: Array String -> Effect Unit
process args =
  let
    result = do
      code <- args !! 2
      selection <- find (\l -> l.id  == code) languages
      pure selection
  in
    case result of
      Nothing -> do
        log $ "A single argument with a language code was not accurately supplied. Use one of the following codes:"
        log $ intercalate "\n" $ map (\l -> "  " <> l.id <> ": " <> l.name) languages

      Just l ->
        l.f unit


languages :: Array { id :: String, name :: String, f :: Unit -> Effect Unit }
languages =
  [ { id: "calc", name: "Integer calculator", f: \_ -> Main.main Calc.language }
  , { id: "core", name: "Functional core language", f: \_ -> Main.main Core.language }
  , { id: "utl", name: "Untyped lambda calculus", f: \_ -> Main.main UntypedLambda.language }
  ]