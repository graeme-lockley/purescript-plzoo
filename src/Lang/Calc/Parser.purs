module Lang.Calc.Parser
  ( parse
  , parseOpExpression
  ) where

import Prelude (($>), (<*), (*>), ($), bind, pure)

import Control.Alt ((<|>), (<$>))
import Control.Lazy (fix)
import Data.Identity (Identity)
import Data.Either (Either(..))
import Text.Parsing.Parser (Parser, ParseError(..), runParser)
import Text.Parsing.Parser.Expr (OperatorTable, Assoc(..), Operator(..), buildExprParser)
import Text.Parsing.Parser.Combinators ((<?>), between)
import Text.Parsing.Parser.Pos (Position)
import Text.Parsing.Parser.String (eof, whiteSpace)

import Lang.Calc.Errors (Error(..))
import Lang.Calc.Lexer (int, symbol, symbolPosition)
import Lang.Calc.Syntax (Expression(..))


parse :: String -> Either Error Expression
parse text =
  case runParser text parseFile of
    Left (ParseError message position) ->
      Left $ SyntaxError { message, position }

    Right e ->
      Right e


parseFile :: Parser String Expression
parseFile =
  whiteSpace *> parseOpExpression <* eof


parseBinaryOpSymbol :: String -> (Position -> Expression -> Expression -> Expression) -> Parser String (Expression -> Expression -> Expression)
parseBinaryOpSymbol s op = do
  pos <- symbolPosition s
  pure $ op pos


table :: OperatorTable Identity String Expression
table =
  [ [ Prefix (symbol "-" $> Negate)
    ]
  , [ Infix (symbol "*" $> Multiply) AssocLeft
    , Infix (parseBinaryOpSymbol "/" Divide) AssocLeft
    ]
  , [ Infix (symbol "+" $> Plus) AssocLeft
    , Infix (symbol "-" $> Minus) AssocLeft
    ]
  ]


parseOpExpression :: Parser String Expression
parseOpExpression =
  fix allExprs
    where
      allExprs p =
        buildExprParser table (parseTerm p)


parseTerm :: Parser String Expression -> Parser String Expression
parseTerm p =
  parseParens p <|> parseInt <?> "digit, '-' or '('"


parseParens :: Parser String Expression -> Parser String Expression
parseParens p =
  between (symbol "(") (symbol ")") parseOpExpression


parseInt :: Parser String Expression
parseInt =
  Num <$> int
