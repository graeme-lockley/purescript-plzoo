module Lang.Calc.Main
  ( language
  ) where

import Prelude (Unit, bind, discard, pure, show, unit, ($), (<>))

import Control.Monad.State.Trans as StateT
import Data.Either (Either(..))
import Effect (Effect)
import Effect.Console (log)

import Lang.Calc.Eval (eval)
import Lang.Calc.Parser (parse)
import Tools.PLZoo.Main (Language(..))
import Tools.PLZoo.Options (Command(..), CommandResult(..), Options, Setting(..))


language :: Language Unit
language =
  Language
    { textPrompt: "plzoo-calc> "
    , options: options
    , initialState: unit
    , execute: execute
    }


options :: Options Unit
options =
  [ Command "quit" $ Switch quitCmd
  , Command "help" $ Switch helpCmd
  ]


quitCmd :: StateT.StateT Unit Effect CommandResult
quitCmd = do
  StateT.lift $ log "Exiting"
  pure Quit


helpCmd :: StateT.StateT Unit Effect CommandResult
helpCmd = do
  _ <- StateT.lift $ log $
    "The following commands are available:\n\n" <>
    "  :help          Show this help menu\n" <>
    "  :quit          Quit\n"
  pure Continue


execute :: String -> StateT.StateT Unit Effect Unit
execute expression =
  case parse expression of
    Left e ->
      StateT.lift $ log $ "Error: " <> show e

    Right v ->
      case eval v of
        Left e ->
          StateT.lift $ log $ "Error: " <> show e

        Right i ->
          StateT.lift $ log $ show i
