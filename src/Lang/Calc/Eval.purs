module Lang.Calc.Eval
  ( eval
  ) where

import Prelude ((+), (-), (*), (/), (==), ($), bind, negate, pure)

import Data.Either (Either(..))

import Lang.Calc.Errors (Error(..))
import Lang.Calc.Syntax (Expression(..))


eval :: Expression -> Either Error Int
eval (Num n) =
  pure n

eval (Negate e) = do
  v <- eval e
  pure (-v)

eval (Plus e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  pure (v1 + v2)

eval (Minus e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  pure (v1 - v2)

eval (Multiply e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  pure (v1 * v2)

eval (Divide pos e1 e2) = do
  v1 <- eval e1
  v2 <- eval e2
  if (v2 == 0) then
    Left $ DivideByZero pos
  else
    pure (v1 / v2)
