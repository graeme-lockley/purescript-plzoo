module Lang.Calc.Syntax
  ( Expression(..)
  ) where

import Prelude

import Data.Tuple (Tuple(..))
import Text.Parsing.Parser.Pos (Position)


data Expression
  = Num Int
  | Negate Expression
  | Plus Expression Expression
  | Minus Expression Expression
  | Multiply Expression Expression
  | Divide Position Expression Expression


instance showExpressions :: Show (Expression) where
  show e =
    showExpression (-1) e


showExpression :: Int -> Expression -> String
showExpression p e =
  let
    Tuple pp text =
      case e of
        Num n ->
          Tuple 3 $ show n

        Negate e ->
          Tuple 2 $ "-" <> showExpression 2 e

        Plus v1 v2 ->
          Tuple 0 $ showExpression 0 v1 <> " + " <> showExpression 0 v2

        Minus v1 v2 ->
          Tuple 0 $ showExpression 0 v1 <> " - " <> showExpression 0 v2

        Multiply v1 v2 ->
          Tuple 1 $ showExpression 1 v1 <> " * " <> showExpression 1 v2

        Divide _ v1 v2 ->
          Tuple 1 $ showExpression 1 v1 <> " / " <> showExpression 1 v2
  in
    if (pp < p) then
      "(" <> text <> ")"
    else
      text


derive instance eqExpression :: Eq (Expression)
