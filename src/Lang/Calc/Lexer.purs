module Lang.Calc.Lexer
  ( int
  , symbol
  , symbolPosition
  ) where

import Prelude ((<*), ($), bind, pure, (-), (*), (+))

import Data.Array (some)
import Data.Char (toCharCode)
import Data.Char.Unicode (isDigit)
import Data.Foldable (foldl)

import Text.Parsing.Parser (Parser, position)
import Text.Parsing.Parser.String (string, satisfy, whiteSpace)
import Text.Parsing.Parser.Pos (Position)


lexeme :: forall a. Parser String a -> Parser String a
lexeme p = p <* whiteSpace


symbol :: String -> Parser String String
symbol s = lexeme $ string s


symbolPosition :: String -> Parser String Position
symbolPosition s = do
  pos <- position
  t <- symbol s
  pure pos


digit :: Parser String Int
digit = do
  d <- satisfy isDigit
  pure $ toCharCode d - toCharCode '0'


int :: Parser String Int
int = do
  digits <- lexeme $ some digit
  pure $ foldl folder 0 digits
    where folder x d = x * 10 + d
