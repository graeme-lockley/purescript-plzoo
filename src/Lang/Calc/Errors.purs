module Lang.Calc.Errors
  ( Error(..)
  ) where

import Prelude

import Text.Parsing.Parser.Pos (Position(..))


data Error
  = SyntaxError { message :: String, position :: Position }
  | DivideByZero Position


instance showError :: Show Error where
  show (SyntaxError { message: message, position: position }) =
    showPosition position <> ": " <> message

  show (DivideByZero position) =
    showPosition position <> ": Divide by zero"


showPosition :: Position -> String
showPosition (Position { line: line, column: column }) =
   "(" <> (show column) <> ", " <> (show line) <> ")"


derive instance eqError :: Eq Error