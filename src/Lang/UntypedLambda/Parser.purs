module Lang.UntypedLambda.Parser
  ( parse
  , parseExpression
  ) where

import Prelude (($), (<*), (*>), bind, pure)

import Control.Alt ((<|>), (<$>))
import Control.Lazy (fix)
import Data.Either (Either(..))
import Data.List ((:), List(..), some)
import Text.Parsing.Parser (Parser, ParseError(..), runParser)
import Text.Parsing.Parser.Combinators (between, sepBy, try)
import Text.Parsing.Parser.String (eof, whiteSpace)

import Lang.UntypedLambda.Errors (Error(..))
import Lang.UntypedLambda.Lexer (constant, symbol)
import Lang.UntypedLambda.Syntax (Expression(..), Statement(..))


parse :: String -> Either Error (List Statement)
parse text =
  case runParser text parseFile of
    Left (ParseError message position) ->
      Left $ SyntaxError { message, position }

    Right e ->
      Right e


parseFile :: Parser String (List Statement)
parseFile =
  whiteSpace *> sepBy parseStatement (symbol ";") <* eof


parseStatement :: Parser String Statement
parseStatement =
  try parseStatementAssignment <|> parseStatementExpression


parseStatementAssignment :: Parser String Statement
parseStatementAssignment = do
  n <- constant
  _ <- symbol ":="
  e <- parseExpression
  pure $ Assign n e


parseStatementExpression :: Parser String Statement
parseStatementExpression = do
  e <- parseExpression
  pure $ Expression e


parseExpression :: Parser String Expression
parseExpression = do
  terms <- some (fix parseTerm)
  pure $ foldTerms terms
    where
      foldTerms Nil = Variable "error"
      foldTerms (t:Nil) = t
      foldTerms (t:ts) = foldTermsAcc t ts

      foldTermsAcc acc Nil = acc
      foldTermsAcc acc (t:ts) = foldTermsAcc (Apply acc t) ts


parseTerm :: Parser String Expression -> Parser String Expression
parseTerm p =
  parseLambda p <|> parseParens p <|> parseVariable


parseVariable :: Parser String Expression
parseVariable =
  Variable <$> constant


parseLambda :: Parser String Expression -> Parser String Expression
parseLambda p = do
  l <- symbol "\\"
  n <- constant
  d <- symbol "."
  e <- parseExpression
  pure $ Lambda n e


parseParens :: Parser String Expression -> Parser String Expression
parseParens p =
    between (symbol "(") (symbol ")") parseExpression
