module Lang.UntypedLambda.Eval
  ( Environment
  , InterpreterState(..)
  , ReductionStrategy(..)
  , StatementResult
  , statement
  , reduce
  , substitute
  ) where

import Prelude (class Eq, class Show, Unit, bind, discard, pure, show, ($), (/=), (<>), (==), (||))

import Control.Monad.State (State, get, modify_)
import Data.Map (Map, insert, lookup)
import Data.Maybe (Maybe(..))
import Data.Set as Set

import Lang.UntypedLambda.Syntax (Expression(..), Statement(..))


data InterpreterState =
  InterpreterState
    { environment :: Environment
    , strategy :: ReductionStrategy
    }


type Environment =
  Map String Expression


data ReductionStrategy
  = WeakNormalForm
  | WeakHeadNormalForm
  | NormalForm
  | HeadNormalForm

instance showReductionStrategy :: Show (ReductionStrategy) where
  show WeakNormalForm = "Weak Normal Form"
  show WeakHeadNormalForm = "Weak Head Normal Form"
  show NormalForm = "Normal Form"
  show HeadNormalForm = "Head Normal Form"

derive instance eqReductionStrategy :: Eq (ReductionStrategy)


data StatementResult
  = SRExpression Expression
  | SRAssign String

instance showStatementResult :: Show (StatementResult) where
  show (SRExpression expression) = show expression
  show (SRAssign name) = name <> " is defined"


substitute :: String -> Expression -> Expression -> Expression
substitute name value expression =
  case expression of
    Variable n | n == name ->
      value

    Lambda n v | n /= name ->
      let
        valuesFVs =
          fv value

        n' =
          variableName valuesFVs n
      in
        Lambda n' $ substitute name value $ substitute n (Variable n') v

    Apply v1 v2 ->
      Apply (substitute name value v1) (substitute name value v2)

    _ ->
      expression


variableName :: Set.Set String -> String -> String
variableName fvs n =
  if Set.member n fvs then
    variableName fvs (n <> "'")
  else
     n


fv :: Expression -> Set.Set String

fv (Variable c) =
  Set.singleton c

fv (Apply v1 v2) =
  Set.union (fv v1) (fv v2)

fv (Lambda n v) =
  Set.delete n (fv v)


reduce :: Expression -> State InterpreterState Expression

reduce e@(Variable n) = do
  binding <- lookupBinding n
  case binding of
    Nothing -> pure e
    Just e1 -> reduce e1

reduce (Lambda n e) = do
  s <- strategy
  e' <- if (s == NormalForm || s == HeadNormalForm) then
          reduce e
        else
          pure e
  pure $ Lambda n e'

reduce (Apply e1 e2) = do
  s <- strategy
  e1' <- reduce e1
  e2' <- if (s == NormalForm || s == WeakHeadNormalForm) then
           reduce e2
         else
           pure e2
  case e1' of
    Lambda n e ->
      reduce $ substitute n e2' e

    _ -> do
      pure $ Apply e1' e2'


statement :: Statement -> State InterpreterState StatementResult

statement (Expression e) = do
  e' <- reduce e
  pure $ SRExpression e'

statement (Assign n e) = do
  addBinding n e
  pure $ SRAssign n


strategy :: State InterpreterState ReductionStrategy
strategy = do
  InterpreterState state <- get
  pure state.strategy


lookupBinding :: String -> State InterpreterState (Maybe Expression)
lookupBinding n = do
  InterpreterState state <- get
  pure $ lookup n state.environment


addBinding :: String -> Expression -> State InterpreterState Unit
addBinding name value = do
  modify_ (\(InterpreterState state) -> InterpreterState $ state { environment = insert name value state.environment })
