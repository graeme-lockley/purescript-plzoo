module Lang.UntypedLambda.Lexer
  ( constant
  , symbol
  , symbolPosition
  ) where

import Prelude

import Data.Array (some, toUnfoldable)
import Data.Char.Unicode (isDigit, isLetter, isSymbol)
import Data.String.CodeUnits (fromCharArray)

import Text.Parsing.Parser (Parser, position)
import Text.Parsing.Parser.String (string, satisfy, whiteSpace)
import Text.Parsing.Parser.Pos (Position)


lexeme :: forall a. Parser String a -> Parser String a
lexeme p = p <* whiteSpace


symbol :: String -> Parser String String
symbol s = lexeme $ string s


symbolPosition :: String -> Parser String Position
symbolPosition s = do
  pos <- position
  t <- symbol s
  pure pos


constantChar :: Parser String Char
constantChar =
  satisfy (\c -> c /= '\\' && c /= '(' && c /= ')' && c /= '.' && c /= ';' && (isDigit c || isLetter c || isSymbol c || c == '*' || c == '/' || c == '|'))


constant :: Parser String String
constant = do
  constantChars <- lexeme $ some constantChar
  pure $ (fromCharArray <<< toUnfoldable) constantChars
