module Lang.UntypedLambda.Syntax
  ( Expression(..)
  , Statement(..)
  ) where

import Prelude

import Data.Tuple (Tuple(..))


data Expression
  = Variable String
  | Lambda String Expression
  | Apply Expression Expression


instance showExpressions :: Show (Expression) where
  show =
    showExpression 0


showExpression :: Int -> Expression -> String
showExpression p e =
  let
    Tuple pp text =
      case e of
        Variable value ->
          Tuple 2 $ value

        Lambda name expression ->
          Tuple 0 $ "\\" <> name <> ". " <> showExpression 0 expression

        Apply e1 e2 ->
          Tuple 1 $ showExpression 1 e1 <> " " <> showExpression 1 e2
  in
    if (pp <= p) then
      "(" <> text <> ")"
    else
      text

derive instance eqExpression :: Eq (Expression)


data Statement
  = Assign String Expression
  | Expression Expression


instance showStatement :: Show (Statement) where
  show (Assign s e) =
    s <> " := " <> show e

  show (Expression e) =
    show e


derive instance eqStatement :: Eq (Statement)
