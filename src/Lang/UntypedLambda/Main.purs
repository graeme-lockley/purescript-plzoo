module Lang.UntypedLambda.Main
  ( language
  ) where

import Prelude (Unit, bind, discard, pure, show, unit, ($), (<>))

import Control.Monad.State (State, runState)
import Control.Monad.State.Trans as StateT
import Data.Array (fromFoldable)
import Data.Either (Either(..))
import Data.Foldable (for_)
import Data.Functor (map)
import Data.List (List)
import Data.Map (empty, toUnfoldable)
import Data.String.Common (joinWith)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Exception (try)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)

import Lang.UntypedLambda.Eval (InterpreterState(..), ReductionStrategy(..), statement)
import Lang.UntypedLambda.Parser (parse)
import Tools.PLZoo.Main (Language(..))
import Tools.PLZoo.Options (Command(..), CommandResult(..), Options, Setting(..))


language :: Language InterpreterState
language =
  Language
    { textPrompt: "plzoo-untyped-lambda> "
    , options: options
    , initialState: InterpreterState { environment: empty, strategy: HeadNormalForm }
    , execute: execute
    }


options :: Options InterpreterState
options =
  [ Command "quit" $ Switch quitCmd
  , Command "help" $ Switch helpCmd
  , Command "load" $ Argument loadCmd
  , Command "show" $ Switch showCmd
  , Command "strategy" $ Options
      [ Tuple "wnf" $ strategyCmd WeakNormalForm
      , Tuple "whnf" $ strategyCmd WeakHeadNormalForm
      , Tuple "nf" $ strategyCmd NormalForm
      , Tuple "hnf" $ strategyCmd HeadNormalForm
      ]
  ]


quitCmd :: StateT.StateT InterpreterState Effect CommandResult
quitCmd = do
  StateT.lift $ log "Exiting"
  pure Quit


helpCmd :: StateT.StateT InterpreterState Effect CommandResult
helpCmd = do
  _ <- StateT.lift $ log $
    "The following commands are available:\n\n" <>
    "  :help          Show this help menu\n" <>
    "  :load <file>   Loads <file> containing lambda definitions\n" <>
    "  :quit          Quit\n" <>
    "  :show          Show the current state\n" <>
    "  :strategy s    Set the evaluation strategy.  The following options exist for s\n" <>
    "                   - wnf\n" <>
    "                     Weak normal form: shallow and eager\n" <>
    "                   - whnf\n" <>
    "                     Weak head normal form: shallow and lazy\n" <>
    "                   - nf\n" <>
    "                     Normal form: deep and eager\n" <>
    "                   - hnf\n" <>
    "                     Head normal form: deep and lazy\n"
  pure Continue


loadCmd :: String -> StateT.StateT InterpreterState Effect CommandResult
loadCmd filename = do
  readTextFileResult <- StateT.lift $ try $ readTextFile UTF8 filename
  case readTextFileResult of
    Right content ->
      pure $ ProcessContent content

    Left error -> do
      StateT.lift $ log $ "File Load Error: " <> show error
      pure Continue


showCmd :: StateT.StateT InterpreterState Effect CommandResult
showCmd = do
  InterpreterState state <- StateT.get
  let environmentItems = (map (\(Tuple name value) -> name <> " := " <> show value) $ toUnfoldable state.environment) :: List String
  StateT.lift $ log $ joinWith "\n" $ fromFoldable environmentItems
  StateT.lift $ log $ "Reduction strategy: " <> show state.strategy
  pure Continue


strategyCmd :: ReductionStrategy -> StateT.StateT InterpreterState Effect CommandResult
strategyCmd strategy = do
  StateT.modify_ (\(InterpreterState rs) -> InterpreterState $ rs { strategy = strategy } )
  pure Continue


execute :: String -> StateT.StateT InterpreterState Effect Unit
execute text =
  let
    processStatement s = do
      result <- inStateT $ statement s
      StateT.lift $ log $ show result
  in
    case parse text of
      Left e -> do
        StateT.lift $ log $ "Error: " <> show e
        pure unit

      Right statements ->
        for_ statements processStatement


inStateT :: forall a b. State a b -> StateT.StateT a Effect b
inStateT f = do
  state <- StateT.get
  let (Tuple e' state') = runState f state
  StateT.put state'
  pure e'


