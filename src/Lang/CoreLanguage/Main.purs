module Lang.CoreLanguage.Main
  ( REPLState
  , compile'
  , language
  , run'
  ) where

import Prelude (Unit, bind, discard, pure, show, unit, ($), (<>), (==), (/=))

import Control.Monad.State.Trans as StateT
import Data.Either (Either(..))
import Data.Foldable (find, foldl, intercalate)
import Data.Functor (map)
import Data.List ((:), List, filter, last)
import Data.Map (Map, empty, insert, values)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Exception (try)
import Node.Encoding (Encoding(..))
import Node.FS.Sync (readTextFile)
import Prettier.Printer ((<+>), DOC, line, nest, nil, pretty, text)

import Lang.CoreLanguage.Compile (compile)
import Lang.CoreLanguage.Errors as Errors
import Lang.CoreLanguage.Machine (State(..), eval, ppPointer, ppPointerDereference, ppStats, ppState)
import Lang.CoreLanguage.Machine.Heap as Heap
import Lang.CoreLanguage.Parser (parse)
import Lang.CoreLanguage.Syntax (Name(..), Program(..), SCDefn(..), pp)
import Tools.PLZoo.Main (Language(..))
import Tools.PLZoo.Options (Command(..), CommandResult(..), Options, Setting(..))


data REPLState
  = REPLState
      { globals :: Map String (SCDefn Name)
      , showStack :: Boolean
      , showHeap :: Boolean
      , showStats :: Boolean
      }


language :: Language REPLState
language =
  Language
    { textPrompt: "plzoo-core> "
    , options: options
    , initialState: REPLState { globals: empty, showStack: true, showHeap: false, showStats: true }
    , execute: execute
    }


options :: Options REPLState
options =
  [ Command "quit" $ Switch quitCmd
  , Command "help" $ Switch helpCmd
  , Command "load" $ Argument loadCmd
  , Command "set" $ Options
      [ Tuple "showStack" $ setStackCmd true
      , Tuple "hideStack" $ setStackCmd false
      , Tuple "showHeap" $ setHeapCmd true
      , Tuple "hideHeap" $ setHeapCmd false
      , Tuple "showStats" $ setStatsCmd true
      , Tuple "hideStats" $ setStatsCmd false
      ]
  , Command "show" $ Switch showCmd
  ]


quitCmd :: StateT.StateT REPLState Effect CommandResult
quitCmd = do
  StateT.lift $ log "Exiting"
  pure Quit


setStackCmd :: Boolean -> StateT.StateT REPLState Effect CommandResult
setStackCmd setting = do
  StateT.modify_ (\(REPLState rs) -> REPLState $ rs { showStack = setting } )
  pure Continue

setHeapCmd :: Boolean -> StateT.StateT REPLState Effect CommandResult
setHeapCmd setting = do
  StateT.modify_ (\(REPLState rs) -> REPLState $ rs { showHeap = setting } )
  pure Continue

setStatsCmd :: Boolean -> StateT.StateT REPLState Effect CommandResult
setStatsCmd setting = do
  StateT.modify_ (\(REPLState rs) -> REPLState $ rs { showStats = setting } )
  pure Continue


helpCmd :: StateT.StateT REPLState Effect CommandResult
helpCmd = do
  _ <- StateT.lift $ log $
    "The following commands are available:\n\n" <>
    "  :help          Show this help menu\n" <>
    "  :load <file>   Loads <file> containing core language supercombinators\n" <>
    "  :quit          Quit\n" <>
    "  :set           Set the different viewing options:\n" <>
    "                   showStack  show the intermediate and final stacks\n" <>
    "                   hideStack  show only the final stack\n" <>
    "                   showHeap   show the final heap\n" <>
    "                   hideHeap   hide the heap\n" <>
    "                   showStats  show the a collection of execution stats after execution\n" <>
    "                   hideStats  do not show any stats\n" <>
    "  :show          Show the defined supercombinators and the different settings"
  pure Continue


loadCmd :: String -> StateT.StateT REPLState Effect CommandResult
loadCmd filename = do
  readTextFileResult <- StateT.lift $ try $ readTextFile UTF8 filename
  case readTextFileResult of
    Right content -> do
      execute content
      pure Continue

    Left error -> do
      StateT.lift $ log $ "File Load Error: " <> show error
      pure Continue


showCmd :: StateT.StateT REPLState Effect CommandResult
showCmd = do
  REPLState replState <- StateT.get
  _ <- StateT.lift $ log $ pretty 200 $ intercalate (line <> line) $ map pp (values $ replState.globals)
  pure Continue


execute :: String -> StateT.StateT REPLState Effect Unit
execute input =
  case parse input of
    Left e ->
      StateT.lift $ log $ "Error: " <> show e

    Right (Program decls) ->
      let
        main =
          find (\(SCDefn (Name name) _ _) -> name == "main") decls

        toGlobal =
          filter (\(SCDefn (Name name) _ _) -> name /= "main") decls

        includeSC globals scdefn@(SCDefn (Name name) _ _) =
          insert name scdefn globals
      in do
        REPLState replState <- StateT.get
        let globals' = foldl includeSC replState.globals toGlobal
        StateT.modify_ (\(REPLState rs) -> REPLState $ rs { globals = globals' } )
        case main of
          Nothing ->
            pure unit
          Just main' ->
            let
              result = do
                state <- mapLeft Errors.MachineError $ compile $ Program $ main' : values globals'
                result' <- mapLeft Errors.MachineError $ eval state
                pure result'
            in do
              replState' <- StateT.get
              case result of
                Left e -> do
                  StateT.lift $ log $ "Error: " <> show e
                Right i -> do
                  StateT.lift $ log $ pretty 200 $ ppResult replState' i


ppResult :: REPLState -> List State -> DOC
ppResult (REPLState replState) states =
  let
    lastStack =
      last states

    statsResult =
      if replState.showStats then
        fromMaybe nil $ map (\(State item) -> text "Execution Statistics:" <> nest 2 (line <> ppStats item.stats) <> line) lastStack
      else
        nil

    stackResult =
      if replState.showStack then
        text "Stack:" <> nest 2 (line <> intercalate line (map ppState states)) <> line
      else
        fromMaybe nil $ map (\item -> text "Stack:" <> nest 2 (line <> ppState item) <> line) lastStack

    heapResult =
      let
        ppHeap heap =
          let
            heapAddresses =
              (Set.toUnfoldable $ Heap.addresses heap) :: List Heap.Pointer
          in
            text "[" <> nest 2 (intercalate nil $ map (\a -> line <> ppPointer a <+> ppPointerDereference heap a) heapAddresses) <> line <> text "]"
      in
        if replState.showHeap then
          fromMaybe nil $ map (\(State item) -> text "Heap:" <> nest 2 (line <> ppHeap item.heap) <> line) lastStack
        else
          nil
  in
    stackResult <> heapResult <> statsResult


compile' :: String -> Either Errors.Error State
compile' input = do
  program <- parse input
  state <- mapLeft Errors.MachineError $ compile program
  pure $ state


run' :: String -> Either Errors.Error (List State)
run' input = do
  program <- parse input
  state <- mapLeft Errors.MachineError $ compile program
  result <- mapLeft Errors.MachineError $ eval state
  pure $ result


mapLeft :: forall a b c. (a -> c) -> Either a b -> Either c b
mapLeft f (Left a) = Left $ f a
mapLeft f (Right a) = Right a