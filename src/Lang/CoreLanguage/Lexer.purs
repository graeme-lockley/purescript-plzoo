module Lang.CoreLanguage.Lexer
  ( int
  , symbol
  , symbolPosition
  , variable
  ) where

import Prelude ((<<<), (<*), ($), (<>), bind, pure, (-), (*), (+))

import Control.Alt ((<|>))
import Data.Array ((:), many, some, toUnfoldable)
import Data.Char (toCharCode)
import Data.Char.Unicode (isDigit)
import Data.Foldable (foldl)
import Data.Set (Set, empty, insert, member)
import Data.String.CodeUnits (fromCharArray)

import Text.Parsing.Parser (Parser, fail, position)
import Text.Parsing.Parser.Combinators (try)
import Text.Parsing.Parser.String (char, string, satisfy, whiteSpace)
import Text.Parsing.Parser.Token (alphaNum, letter)
import Text.Parsing.Parser.Pos (Position)


lexeme :: forall a. Parser String a -> Parser String a
lexeme p = p <* whiteSpace


symbol :: String -> Parser String String
symbol s = lexeme $ string s


symbolPosition :: String -> Parser String Position
symbolPosition s = do
  pos <- position
  t <- symbol s
  pure pos


digit :: Parser String Int
digit = do
  d <- satisfy isDigit
  pure $ toCharCode d - toCharCode '0'


keywords :: Set String
keywords =
  insert "of" $
  insert "in" $
  insert "let" $
  insert "letrec" $
  insert "case" $
  insert "Pack" $
  empty


variable :: Parser String String
variable = try $ do
  first <- letter <|> char '_'
  rest <- lexeme $ many (letter <|> alphaNum <|> char '_')
  let value = charArrayToString $ first : rest
  if member value keywords then
    fail ("Keyword: " <> value)
  else
    pure value


int :: Parser String Int
int = do
  digits <- lexeme $ some digit
  pure $ foldl folder 0 digits
    where folder x d = x * 10 + d


charArrayToString :: Array Char -> String
charArrayToString =
  fromCharArray <<< toUnfoldable