module Lang.CoreLanguage.Errors
  ( Error(..)
  ) where

import Prelude

import Text.Parsing.Parser.Pos (Position(..))

import Lang.CoreLanguage.Machine.Errors as MachineErrors


data Error
  = SyntaxError { message :: String, position :: Position }
  | MachineError MachineErrors.Error


instance showError :: Show Error where
  show (SyntaxError { message: message, position: position }) =
    showPosition position <> ": " <> message

  show (MachineError e) =
    show e


showPosition :: Position -> String
showPosition (Position { line: line, column: column }) =
   "(" <> (show column) <> ", " <> (show line) <> ")"


derive instance eqError :: Eq Error