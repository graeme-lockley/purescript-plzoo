module Lang.CoreLanguage.Machine
  ( Dump
  , Globals
  , Node(..)
  , Primitive(..)
  , State(..)
  , Stats(..)
  , eval
  , initialStats
  , ppPointer
  , ppPointerDereference
  , ppState
  , ppStats
  , setup
  ) where

import Prelude (class Show, (+), (-), (*), (/), ($), (<>), Unit, bind, discard, negate, not, pure, show)

import Control.Comonad (extract)
import Control.Monad.Except.Trans (ExceptT, runExceptT, throwError)
import Control.Monad.State.Trans (StateT, get, put, runStateT)
import Control.Monad (class Monad)
import Data.Either (Either(..), note')
import Data.Foldable (for_, intercalate)
import Data.Functor (map)
import Data.Identity (Identity)
import Data.List ((:), List(..), drop, length, null, singleton, zip)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Prettier.Printer (DOC, line, nest, pretty, text, (<+>))

import Lang.CoreLanguage.Syntax (CoreExpr, Expr(..), IsRec(..), Name(..), pp)
import Lang.CoreLanguage.Machine.Heap (Heap, Pointer, alloc, lookup, update)
import Lang.CoreLanguage.Machine.Errors (Error(..))


data Node
  = NAp Pointer Pointer
  | NSupercomb Name (List Name) CoreExpr
  | NNum Int
  | NInd Pointer
  | NPrim String Primitive

data Primitive
  = Neg
  | Add
  | Sub
  | Mul
  | Div

isDataNode :: Node -> Boolean
isDataNode (NNum _) = true
isDataNode _ = false


type Dump
  = List (List Pointer)


type Globals
  = Map.Map Name Pointer


data Stats
  = Stats Int

ppStats :: Stats -> DOC
ppStats (Stats n) =
  text (show n)


initialStats :: Stats
initialStats =
  Stats 0


incSteps :: Stats -> Stats
incSteps (Stats n) =
  Stats $ n + 1


data State
  = State
      { stack :: List Pointer
      , dump :: Dump
      , heap :: Heap Node
      , globals :: Globals
      , stats :: Stats
      }

instance showState :: Show (State) where
  show s = pretty 250 $ ppState s


type MachineState =
  StateT State (ExceptT Error Identity)


ppState :: State -> DOC
ppState (State state) =
  text "[" <> nest 2 (line <> intercalate line (map (ppStackItem state.heap) state.stack)) <> line <> text "]"

ppStackItem :: Heap Node -> Pointer -> DOC
ppStackItem heap pointer =
  ppPointer pointer <> text ": " <> ppStackNode heap pointer

ppStackNode :: Heap Node -> Pointer -> DOC
ppStackNode heap p =
  case lookup p heap of
    Nothing ->
      text "illegal address"

    Just (NAp f a) ->
      text "NAp" <+> ppPointer f <+> ppPointer a <+> text "(" <> ppPointerDereference heap f <> text "," <+> ppPointerDereference heap a <> text ")"

    Just node ->
      ppNode node

ppNode :: Node -> DOC
ppNode (NAp a1 a2) = text "NAp" <+> ppPointer a1 <+> ppPointer a2
ppNode (NSupercomb name _ _) = text "NSupercomb" <+> pp name
ppNode (NNum n) = text "NNum" <+> text (show n)
ppNode (NInd p) = text "Nind" <+> ppPointer p
ppNode (NPrim n primitive) = text "NPrim" <+> text n

ppPointer :: Pointer -> DOC
ppPointer pointer =
  text $ show pointer

ppPointerDereference :: Heap Node -> Pointer -> DOC
ppPointerDereference heap p =
  case lookup p heap of
    Nothing ->
      text "illegal address"

    Just node ->
      ppNode node


mapStats :: (Stats -> Stats) -> State -> State
mapStats f (State state) =
  State $ state { stats = f state.stats }


setup :: Heap Node -> Globals -> Either Error State
setup heap globals = do
  mainPointer <- note' (\_ -> UnknownName $ "main") $ Map.lookup (Name "main") globals
  pure $ State
    { stack: singleton mainPointer
    , dump: Nil
    , heap: heap
    , globals: globals
    , stats: initialStats
    }


eval :: State -> Either Error (List State)
eval state = do
  finished <- isFinal state
  if finished then
    pure $ state : Nil
  else do
    let next = extract $ runExceptT $ runStateT step state
    case next of
      Left e -> Left e
      Right (Tuple _ nextState) -> do
        rest <- eval $ doAdmin nextState
        pure $ state : rest



isFinal :: State -> Either Error Boolean
isFinal state =
  case state of
    State { stack: soleAddress : Nil, dump: Nil, heap: heap } -> do
      node <- note' (\_ -> InvalidPointer $ show soleAddress) $ lookup soleAddress heap
      pure $ isDataNode node

    State { stack: Nil } ->
      Left EmptyStack

    _ ->
      pure false


doAdmin :: State -> State
doAdmin =
  mapStats incSteps


step ::  MachineState Unit
step = do
  pointer <- head
  instruction <- dereferencePointer pointer
  dispatch instruction
    where dispatch (NNum n) =
            numStep n

          dispatch (NAp a1 a2) =
            apStep a1 a2

          dispatch (NSupercomb name args body) = do
            includeArgBindings args
            dropStack (length args)
            p <- head
            instantiateAndUpdate p body

          dispatch (NInd a) = do
            dropStack 1
            pushOntoStack a

          dispatch (NPrim name primitive) =
            primStep primitive name


numStep :: Int -> MachineState Unit
numStep _ = do
  isEmpty <- isDumpEmpty
  if isEmpty then
    throwError NumberAppliedAsFunction
  else
    popDump


apStep :: Pointer -> Pointer -> MachineState Unit
apStep a1 a2 = do
  node <- dereferencePointer a2
  apDispatch node
    where apDispatch (NInd a') = do
            p <- head
            updateHeap p (NAp a1 a')
          apDispatch _ =
            pushOntoStack a1


primStep :: Primitive -> String -> MachineState Unit
primStep Neg = \_ -> primNeg
primStep Add = primArith (+)
primStep Sub = primArith (-)
primStep Mul = primArith (*)
primStep Div = primArith (/)


primNeg :: MachineState Unit
primNeg = do
  args <- getArgs
  case args of
    argAddress : Nil -> do
      argNode <- dereferencePointer argAddress
      dropStack 1

      if isDataNode argNode then do
        argValue <- getNNumValue argNode
        rootOfRedex <- head
        updateHeap rootOfRedex $ NNum $ -argValue
      else
        pushStack $ singleton argAddress

    _ ->
      throwError $ BuiltInCardinality "negate" 1 $ length args


primArith :: (Int -> Int -> Int) -> String -> MachineState Unit
primArith op name = do
  args <- getArgs
  case args of
    arg1Address : arg2Address : Nil -> do
      arg1Node <- dereferencePointer arg1Address
      arg2Node <- dereferencePointer arg2Address
      dropStack 2

      if not isDataNode arg1Node then
        pushStack $ singleton arg1Address
      else if not isDataNode arg2Node then
        pushStack $ singleton arg2Address
      else do
        arg1Value <- getNNumValue arg1Node
        arg2Value <- getNNumValue arg2Node
        rootOfRedex <- head
        updateHeap rootOfRedex $ NNum $ op arg1Value arg2Value

    _ ->
      throwError $ BuiltInCardinality name 1 $ length args



getNNumValue :: Node -> MachineState Int
getNNumValue (NNum n) =
  pure n
getNNumValue n =
  throwError $ HeapNodeNotNum $ pretty 250 $ ppNode n


getArgs :: MachineState (List Pointer)
getArgs =
  let
    mapPointer p = do
      node <- dereferencePointer p
      case node of
        NAp _ p' -> do
          pure $ p'
        _ ->
          throwError $ HeapNodeNotAp $ "getArgs: " <> show p
  in do
    ss <- stack
    mapM mapPointer $ drop 1 $ ss


mapM :: forall a b m. Monad m => (a -> m b) -> List a -> m (List b)
mapM f Nil = pure Nil
mapM f (x:xs) = do
  h <- f x
  rest <- mapM f xs
  pure $ h : rest


includeArgBindings :: List Name -> MachineState Unit
includeArgBindings names = do
  args <- getArgs
  let bindings = Map.fromFoldable $ zip names args
  includeBindings bindings


instantiate :: CoreExpr -> MachineState Pointer
instantiate (ENum n) =
  allocHeap $ NNum n
instantiate (EVar n) =
  lookupBinding n
instantiate (EAp e1 e2) = do
  a1 <- instantiate e1
  a2 <- instantiate e2
  allocHeap $ NAp a1 a2
instantiate (EConstr _ _) =
  throwError EConstrNotImplemented
instantiate (ELet NonRecursive bindings body)  = do
  instantiateNonRecursiveBindings bindings
  instantiate body
instantiate (ELet Recursive bindings body) = do
  pointerExpressions <- allocateRecursiveBindings bindings
  for_ pointerExpressions (\(Tuple p' expr) -> instantiateAndUpdate p' expr)
  instantiate body
instantiate (ECase _ _) =
  throwError ECaseNotImplemented
instantiate (ELam _ _) =
  throwError ELamNotImplemented


instantiateAndUpdate :: Pointer -> CoreExpr -> MachineState Unit
instantiateAndUpdate p (ENum n) =
  updateHeap p $ NNum n
instantiateAndUpdate p (EVar n) = do
  pointer <- lookupBinding n
  updateHeap p $ NInd pointer
instantiateAndUpdate p (EAp e1 e2) = do
  a1 <- instantiate e1
  a2 <- instantiate e2
  updateHeap p $ NAp a1 a2
instantiateAndUpdate _ (EConstr _ _) =
  throwError EConstrNotImplemented
instantiateAndUpdate p (ELet NonRecursive bindings body) = do
  instantiateNonRecursiveBindings bindings
  instantiateAndUpdate p body
instantiateAndUpdate p (ELet Recursive bindings body) = do
  pointerExpressions <- allocateRecursiveBindings bindings
  for_ pointerExpressions (\(Tuple p' expr) -> instantiateAndUpdate p' expr)
  instantiateAndUpdate p body
instantiateAndUpdate p (ECase _ _) =
  throwError ECaseNotImplemented
instantiateAndUpdate p (ELam _ _) =
  throwError ELamNotImplemented


instantiateNonRecursiveBindings :: List (Tuple Name (Expr Name)) -> MachineState Unit
instantiateNonRecursiveBindings bindings =
  for_ bindings instantiateBinding
    where instantiateBinding (Tuple name expr) = do
            p <- instantiate expr
            includeBinding name p


allocateRecursiveBindings :: List (Tuple Name (Expr Name)) -> MachineState (List (Tuple Pointer (Expr Name)))
allocateRecursiveBindings =
  mapM allocateRecursiveBinding
    where allocateRecursiveBinding (Tuple name expr) = do
            p <- allocHeap $ NNum 0
            includeBinding name p
            pure $ Tuple p expr


dereferencePointer :: Pointer -> MachineState Node
dereferencePointer p = do
  State state <- get
  case lookup p state.heap of
    Nothing ->
      throwError $ InvalidPointer $ show p

    Just node ->
      pure node


dropStack :: Int -> MachineState Unit
dropStack n = do
  State state <- get
  put $ State $ state { stack = drop n state.stack }


pushOntoStack :: Pointer -> MachineState Unit
pushOntoStack p = do
  State state <- get
  put $ State $ state { stack = p : state.stack }


head :: MachineState Pointer
head = do
  State state <- get
  case state.stack of
    Nil ->
      throwError EmptyStack

    x : _ ->
      pure x


stack :: MachineState (List Pointer)
stack = do
  State state <- get
  pure state.stack


pushStack :: List Pointer -> MachineState Unit
pushStack newStack = do
  State state <- get
  put $ State $ state { stack = newStack, dump = state.stack : state.dump }


updateHeap :: Pointer -> Node -> MachineState Unit
updateHeap p n = do
  State state <- get
  put $ State $ state { heap = update p n state.heap }


allocHeap :: Node -> MachineState Pointer
allocHeap n = do
  State state <- get
  let Tuple p heap' = alloc n state.heap
  put $ State $ state { heap = heap' }
  pure p


includeBindings :: Map.Map Name Pointer -> MachineState Unit
includeBindings bindings = do
  State state <- get
  put $ State $ state { globals = Map.union bindings state.globals }


includeBinding :: Name -> Pointer -> MachineState Unit
includeBinding name pointer = do
  State state <- get
  put $ State $ state { globals = Map.insert name pointer state.globals }


lookupBinding :: Name -> MachineState Pointer
lookupBinding name = do
  State state <- get
  case Map.lookup name state.globals of
    Nothing ->
      throwError $ UnknownName $ show name
    Just p ->
      pure p


isDumpEmpty :: MachineState Boolean
isDumpEmpty = do
  State state <- get
  pure $ null state.dump


popDump :: MachineState Unit
popDump = do
  state <- get
  case state of
    State { dump: Nil } ->
      throwError EmptyStack

    State s@{ dump: stack' : dump } ->
      put $ State $ s { stack = stack', dump = dump }
