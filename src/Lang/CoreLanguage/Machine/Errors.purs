module Lang.CoreLanguage.Machine.Errors
  ( Error(..)
  ) where

import Prelude


data Error
  = UnknownName String
  | EmptyStack
  | InvalidPointer String
  | NumberAppliedAsFunction
  | HeapNodeNotAp String
  | HeapNodeNotNum String
  | EConstrNotImplemented
  | ELetNotImplemented
  | ECaseNotImplemented
  | ELamNotImplemented
  | PrimNotImplemented
  | BuiltInCardinality String Int Int


instance showError :: Show Error where
  show (UnknownName n) = "Unknown Name: " <> n
  show EmptyStack = "Empty Stack"
  show (InvalidPointer n) = "Invalid Pointer: " <> n
  show NumberAppliedAsFunction = "Number Applied as Function"
  show (HeapNodeNotAp n) = "Heap Node Not Ap: " <> n
  show (HeapNodeNotNum n) = "Heap Node Not Num: " <> n
  show EConstrNotImplemented = "EConstr Not Implemented"
  show ELetNotImplemented = "ELet Not Implemented"
  show ECaseNotImplemented = "ECase Not Implemented"
  show ELamNotImplemented = "ELam Not Implemented"
  show PrimNotImplemented = "Prim Not Implemented"
  show (BuiltInCardinality n e a) = "Built In Cardinality: function " <> n <> " expected " <> show e <> " arguments but received " <> show a


derive instance eqError :: Eq Error