module Lang.CoreLanguage.Machine.Heap
  ( Heap
  , Pointer
  , addresses
  , alloc
  , free
  , initial
  , lookup
  , size
  , update
  ) where

import Prelude (class Eq, class Ord, class Show, (+), (-), ($), (<>), compare, show)

import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.String (length, take)
import Data.Tuple (Tuple(..))


data Pointer
  = Pointer Int

derive instance eqPointer :: Eq (Pointer)

instance showPointer :: Show (Pointer) where
  show (Pointer n) =
    let
      full =
        "0000"
      value =
        show  n
    in
      "X" <> take (4 - length value) full <> value

instance ordPointer :: Ord (Pointer) where
  compare (Pointer a) (Pointer b) =
    compare a b


data Heap a
  = Heap
      { free :: Set.Set Int
      , next :: Int
      , nodes :: Map.Map Int a
      }


initial :: forall a. Heap a
initial =
  Heap { free: Set.empty, next: 0, nodes: Map.empty }


alloc :: forall a. a -> Heap a -> Tuple Pointer (Heap a)
alloc node (Heap heap) =
  let
    address =
      Set.findMin heap.free
  in
    case address of
      Nothing ->
        Tuple (Pointer heap.next) $ Heap { free: Set.empty, next: heap.next + 1, nodes: Map.insert heap.next node heap.nodes }

      Just a ->
        Tuple (Pointer a) $ Heap { free: Set.delete a heap.free, next: heap.next, nodes: Map.insert a node heap.nodes }

update :: forall a. Pointer -> a -> Heap a -> Heap a
update (Pointer address) node (Heap heap) =
  Heap { free: heap.free, next: heap.next, nodes: Map.insert address node heap.nodes }


free :: forall a. Pointer -> Heap a -> Heap a
free (Pointer address) (Heap heap) =
  Heap { free: Set.insert address heap.free, next: heap.next, nodes: Map.delete address heap.nodes }


lookup :: forall a. Pointer -> Heap a -> Maybe a
lookup (Pointer address) (Heap heap) =
  Map.lookup address heap.nodes


size :: forall a. Heap a -> Int
size (Heap heap) =
  Map.size heap.nodes


addresses :: forall a. Heap a -> Set.Set Pointer
addresses (Heap heap) =
  Set.map Pointer $ Map.keys heap.nodes



{-

module Lang.CoreLanguage.Machine.Heap
  ( Heap
  , Pointer
  , addresses
  , alloc
  , free
  , initial
  , lookup
  , size
  , update
  ) where

import Prelude (class Eq, class Ord, class Show, (+), (-), ($), (<>), (/=), (==), compare, show)

import Data.Functor (map)
import Data.List ((:), List(..), filter, find, head, length, tail)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String as S
import Data.Tuple (Tuple(..), fst, snd)


data Pointer
  = Pointer Int

derive instance eqPointer :: Eq (Pointer)

instance showPointer :: Show (Pointer) where
  show (Pointer n) =
    let
      full =
        "0000"
      value =
        show  n
    in
      "X" <> S.take (4 - S.length value) full <> value

instance ordPointer :: Ord (Pointer) where
  compare (Pointer a) (Pointer b) =
    compare a b


data Heap a
  = Heap
      { free :: List Int
      , next :: Int
      , nodes :: List (Tuple Int a)
      }


initial :: forall a. Heap a
initial =
  Heap { free: Nil, next: 0, nodes: Nil }


alloc :: forall a. a -> Heap a -> Tuple Pointer (Heap a)
alloc node (Heap heap) =
  let
    address =
      head heap.free
  in
    case address of
      Nothing ->
        Tuple (Pointer heap.next) $ Heap { free: Nil, next: heap.next + 1, nodes: (Tuple heap.next node) : heap.nodes }

      Just a ->
        Tuple (Pointer a) $ Heap { free: fromMaybe Nil $ tail heap.free, next: heap.next, nodes: (Tuple a node) : heap.nodes }

update :: forall a. Pointer -> a -> Heap a -> Heap a
update (Pointer address) node (Heap heap) =
  Heap { free: heap.free, next: heap.next, nodes: (Tuple address node) : filter (\(Tuple a n) -> a /= address) heap.nodes }


free :: forall a. Pointer -> Heap a -> Heap a
free (Pointer address) (Heap heap) =
  Heap { free: address : heap.free, next: heap.next, nodes: filter (\(Tuple a n) -> a /= address) heap.nodes }


lookup :: forall a. Pointer -> Heap a -> Maybe a
lookup (Pointer address) (Heap heap) =
  map snd $ find (\(Tuple a n) -> a == address) heap.nodes


size :: forall a. Heap a -> Int
size (Heap heap) =
  length heap.nodes


addresses :: forall a. Heap a -> List Pointer
addresses (Heap heap) =
  map Pointer $ map fst heap.nodes


-}
