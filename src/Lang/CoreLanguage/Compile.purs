module Lang.CoreLanguage.Compile
  ( compile
  ) where

import Prelude (($))
import Data.Either (Either)
import Data.Foldable (foldl)
import Data.List (List)
import Data.Map as Map
import Data.Tuple (Tuple(..))

import Lang.CoreLanguage.Machine as Machine
import Lang.CoreLanguage.Machine.Errors (Error)
import Lang.CoreLanguage.Machine.Heap as Heap
import Lang.CoreLanguage.Syntax (CoreProgram, Name(..), Program(..), SCDefn(..))


compile :: CoreProgram -> Either Error Machine.State
compile (Program program) =
  let
    allocateSc (Tuple heap globals) (SCDefn n args expr) =
      let
        Tuple address heap' =
          Heap.alloc (Machine.NSupercomb n args expr) heap
      in
        Tuple heap' (Map.insert n address globals)

    allocatePrim (Tuple heap globals) (Tuple name primitive) =
     let
       Tuple address heap' =
         Heap.alloc (Machine.NPrim name primitive) heap
     in
       Tuple heap' (Map.insert (Name name) address globals)

    Tuple heap1 globals1 =
      foldl allocateSc (Tuple Heap.initial Map.empty) program

    Tuple heap2 globals2 =
      foldl allocatePrim (Tuple heap1 globals1) ((Map.toUnfoldable primitives) :: List (Tuple String Machine.Primitive))
  in
    Machine.setup heap2 globals2


primitives :: Map.Map String Machine.Primitive
primitives =
  Map.insert "negate" Machine.Neg $
  Map.insert "+" Machine.Add $
  Map.insert "-" Machine.Sub $
  Map.insert "*" Machine.Mul $
  Map.insert "/" Machine.Div $
  Map.empty