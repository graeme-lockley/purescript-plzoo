module Lang.CoreLanguage.Syntax
  ( class PrettyPrint
  , Alter(..)
  , CoreExpr
  , CoreProgram
  , CoreSCDefn
  , Expr(..)
  , IsRec(..)
  , Name(..)
  , Program(..)
  , SCDefn(..)
  , bindersOf
  , isAtomicExpr
  , pp
  , rhssOf
  ) where

import Prelude (class Eq, class Ord, class Show, ($), (<=), (<>), show)

import Data.Foldable (intercalate)
import Data.Functor (map)
import Data.List (List(..))
import Data.Tuple (Tuple(..), fst, snd)
import Prettier.Printer


class PrettyPrint p where
  pp :: p -> DOC


data Program n
  = Program (List (SCDefn n))

type CoreProgram
  = Program Name

instance showProgram :: PrettyPrint n => Show (Program n) where
  show p = pretty 250 $ pp p

derive instance eqProgram :: Eq n => Eq (Program n)

instance ppProgram :: PrettyPrint n => PrettyPrint (Program n) where
  pp (Program scdefns) =
    intercalate (text " ;" <> line <> line) $ map pp scdefns


data SCDefn n
  = SCDefn Name (List n) (Expr n)

type CoreSCDefn
  = SCDefn Name

derive instance eqSCDefn :: Eq n => Eq (SCDefn n)

instance ppSCDefn :: PrettyPrint n => PrettyPrint (SCDefn n) where
  pp (SCDefn name Nil expr) =
    pp name <+> text "=" <> nest 2 (line <> pp expr)
  pp (SCDefn name vars expr) =
    pp name <+> intercalate (text " ") (map pp vars) <+> text "=" <> nest 2 (line <> pp expr)


data Expr n
  = EVar Name
  | ENum Int
  | EConstr Int Int
  | EAp (Expr n) (Expr n)
  | ELet IsRec (List (Tuple n (Expr n))) (Expr n)
  | ECase (Expr n) (List (Alter n))
  | ELam (List n) (Expr n)

type CoreExpr
  = Expr Name

derive instance eqExpr :: Eq n => Eq (Expr n)

instance ppExpr :: PrettyPrint n => PrettyPrint (Expr n) where
  pp expr =
    let
      pp' precedence e =
        let
          endOfLineSeparator =
            text " ;" <> line

          ppLet label decls e' =
            Tuple 7 $ text label <> nest 2 (line <> (intercalate endOfLineSeparator (map (\(Tuple f s) -> pp f <+> text "=" <> nest 2 (line <> pp s)) decls))) <> line <> text "in" <> nest 2 (line <> pp e')

          Tuple precedence' doc =
            case e of
              EVar (Name n) ->
                Tuple 7 $ text n
              ENum n ->
                Tuple 7 $ text $ show n
              EConstr tag arity ->
                Tuple 7 $ text "Pack{" <> text (show tag) <> text ", " <> text (show arity) <> text "}"
              EAp e1 e2 ->
                Tuple 6 $ pp' 5 e1 <> text " " <> pp' 6 e2
              ELet NonRecursive decls expr ->
                ppLet "let" decls expr
              ELet Recursive decls expr ->
                ppLet "letrec" decls expr
              ECase e' alterns ->
                Tuple 7 $ text "case" <+> pp e' <+> text "of" <> nest 2 (line <> (intercalate endOfLineSeparator $ map pp alterns))
              ELam args expr ->
                Tuple 7 $ text "\\" <> spread (map pp args) <> text "." <+> pp expr
        in
          if (precedence' <= precedence) then
            text "(" <> doc <> text ")"
          else
            doc
    in
      pp' 0 expr

data Alter n
  = Alter Int (List n) (Expr n)

type CoreAlter
  = Alter Name

derive instance eqAlter :: Eq n => Eq (Alter n)

instance ppAlter :: PrettyPrint n => PrettyPrint (Alter n) where
  pp (Alter tag Nil expr) =
    text "<" <> text (show tag) <> text "> -> " <> pp expr
  pp (Alter tag vars expr) =
    text "<" <> text (show tag) <> text ">" <> intercalate (text " ") (map pp vars) <> text " -> " <> pp expr


data Name
  = Name String

derive instance eqName :: Eq (Name)

derive instance ordName :: Ord (Name)

instance ppName :: PrettyPrint (Name) where
  pp (Name n) = text n

instance showName :: Show (Name) where
  show (Name s) = s


data IsRec
  = Recursive
  | NonRecursive

derive instance eqIsRec :: Eq (IsRec)


bindersOf :: forall a b. List (Tuple a b) -> List a
bindersOf =
  map fst


rhssOf :: forall a b. List (Tuple a b) -> List b
rhssOf =
  map snd


isAtomicExpr :: forall a. Expr a -> Boolean
isAtomicExpr (EVar _) = true
isAtomicExpr (ENum _) = true
isAtomicExpr _ = false
