module Lang.CoreLanguage.Parser
  ( parse
  ) where

import Prelude ((<<<), ($>), (<*), (*>), ($), bind, pure)

import Control.Alt ((<|>), (<$>))
import Control.Lazy (fix)
import Data.Identity (Identity)
import Data.Either (Either(..))
import Data.List (List, foldl, many)
import Data.Tuple (Tuple(..))
import Text.Parsing.Parser (Parser, ParseError(..), runParser)
import Text.Parsing.Parser.Expr (OperatorTable, Assoc(..), Operator(..), buildExprParser)
import Text.Parsing.Parser.Combinators ((<?>), between, sepBy)
import Text.Parsing.Parser.String (eof, whiteSpace)

import Lang.CoreLanguage.Errors (Error(..))
import Lang.CoreLanguage.Lexer (int, symbol, variable)
import Lang.CoreLanguage.Syntax


parse :: String -> Either Error CoreProgram
parse text =
  case runParser text parseFile of
    Left (ParseError message position) ->
      Left $ SyntaxError { message, position }

    Right e ->
      Right $ Program e


parseFile :: Parser String (List CoreSCDefn)
parseFile =
  whiteSpace *> sepBy parseSuperCombinator (symbol ";") <* eof


parseSuperCombinator :: Parser String CoreSCDefn
parseSuperCombinator = do
  var <- parseVariable
  variables <- many parseVariable
  _ <- symbol "="
  expr <- parseExpr
  pure $ SCDefn var variables expr


parseExpr :: Parser String CoreExpr
parseExpr =
  fix pe
    where pe p = parseExpr1 p


parseExpr1 :: Parser String CoreExpr -> Parser String CoreExpr
parseExpr1 p =
  parseCaseExpr p <|>
  parseLetrecExpr p <|>
  parseLetExpr p <|>
  parseLambda p <|>
  parseExpr2 p


parseLetExpr :: Parser String CoreExpr -> Parser String CoreExpr
parseLetExpr p = do
  _ <- symbol "let"
  defns <- parseDefns p
  _ <- symbol "in"
  expr <- parseExpr1 p
  pure $ ELet NonRecursive defns expr


parseLetrecExpr :: Parser String CoreExpr -> Parser String CoreExpr
parseLetrecExpr p = do
  _ <- symbol "letrec"
  defns <- parseDefns p
  _ <- symbol "in"
  expr <- parseExpr1 p
  pure $ ELet Recursive defns expr


parseCaseExpr :: Parser String CoreExpr -> Parser String CoreExpr
parseCaseExpr p = do
  _ <- symbol "case"
  expr <- parseExpr1 p
  _ <- symbol "of"
  alts <- parseAlts p
  pure $ ECase expr alts


parseLambda :: Parser String CoreExpr -> Parser String CoreExpr
parseLambda p = do
  _ <- symbol "\\"
  vars <- many parseVariable
  _ <- symbol "."
  expr <- parseExpr1 p
  pure $ ELam vars expr


table :: OperatorTable Identity String CoreExpr
table =
  let
    mkOp symbol left right =
      EAp (EAp (EVar $ Name symbol) left) right
  in
    [ [ Infix (symbol "*" $> mkOp "*") AssocRight
      , Infix (symbol "/" $> mkOp "/") AssocNone
      ]
    , [ Infix (symbol "+" $> mkOp "+") AssocRight
      , Infix (symbol "-" $> mkOp "-") AssocNone
      ]
    , [ Infix (symbol "==" $> mkOp "==") AssocNone
      , Infix (symbol "~=" $> mkOp "~=") AssocNone
      , Infix (symbol ">" $> mkOp ">") AssocNone
      , Infix (symbol ">=" $> mkOp ">=") AssocNone
      , Infix (symbol "<" $> mkOp "<") AssocNone
      , Infix (symbol "<=" $> mkOp "<=") AssocNone
      ]
    , [ Infix (symbol "&" $> mkOp "&") AssocRight
      ]
    , [ Infix (symbol "|" $> mkOp "|") AssocRight
      ]
    ]


parseExpr2 :: Parser String CoreExpr -> Parser String CoreExpr
parseExpr2 p =
  buildExprParser table (parseExpr3 p)


parseExpr3 :: Parser String CoreExpr -> Parser String CoreExpr
parseExpr3 p = do
  left <- parseTerm p
  params <- many (parseTerm p)
  pure $ foldl EAp left params


parseTerm :: Parser String CoreExpr -> Parser String CoreExpr
parseTerm p =
  parseParens p
  <|> parsePack
  <|> parseInt
  <|> parseVar
  <?> "digit, variable, '-' or '('"


parseParens :: Parser String CoreExpr -> Parser String CoreExpr
parseParens p =
  between (symbol "(") (symbol ")") parseExpr


parseInt :: Parser String CoreExpr
parseInt =
  ENum <$> int


parseVar :: Parser String CoreExpr
parseVar =
  EVar <<< Name <$> variable


parsePack :: Parser String CoreExpr
parsePack = do
  _ <- symbol "Pack"
  _ <- symbol "{"
  tag <- int
  _ <- symbol ","
  arity <- int
  _ <- symbol "}"
  pure $ EConstr tag arity


parseDefns :: Parser String CoreExpr -> Parser String (List (Tuple Name (Expr Name)))
parseDefns p =
  sepBy (parseDefn p) (symbol ";")


parseDefn :: Parser String CoreExpr -> Parser String (Tuple Name (Expr Name))
parseDefn p = do
  var <- parseVariable
  _ <- symbol "="
  expr <- parseExpr1 p
  pure $ Tuple var expr


parseAlts :: Parser String CoreExpr -> Parser String (List (Alter Name))
parseAlts p =
  sepBy (parseAlt p) (symbol ";")


parseAlt :: Parser String CoreExpr -> Parser String (Alter Name)
parseAlt p = do
  _ <- symbol "<"
  tag <- int
  _ <- symbol ">"
  vars <- many parseVariable
  _ <- symbol "->"
  expr <- parseExpr1 p
  pure $ Alter tag vars expr


parseVariable :: Parser String Name
parseVariable = do
  var <- variable
  pure $ Name var
