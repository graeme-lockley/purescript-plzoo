module Tools.PLZoo.Main
  ( Language(..)
  , main
  ) where

import Prelude (($), (==), (<>), Unit, bind, discard, pure, show)

import Control.Monad.State.Trans as StateT
import Data.Either (Either(..))
import Data.String (length, take)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Exception (try)
import Node.Readline (createInterface, prompt, setHandler, setPrompt)
import Node.Readline.Action (Action(..))

import Tools.PLZoo.Options (CommandResult(..), Options, execute)


data Language interpreterState
  = Language
      { textPrompt :: String
      , options :: Options interpreterState
      , initialState :: interpreterState
      , execute :: String -> StateT.StateT interpreterState Effect Unit
      }


main :: forall interpreterState. Language interpreterState -> Effect Unit
main (Language language) =
  let
    handler :: String -> interpreterState -> Effect (Tuple Action interpreterState)
    handler s env =
      if startsWith ":" s then
        do
          callResult <- StateT.runStateT (execute language.options s) env
          case callResult of
            Tuple (Left error) env' -> do
             log $ "Error: " <> show error
             pure $ Tuple Prompt env'

            Tuple (Right Quit) env' ->
              pure $ Tuple Close env'

            Tuple (Right Continue) env' ->
              pure $ Tuple Prompt env'

            Tuple (Right (ProcessContent content)) newEnv ->
              handler content newEnv
      else
        do
          callResult <- try $ StateT.runStateT (language.execute s) env
          case callResult of
            Left error -> do
             log $ "Error: " <> show error
             pure $ Tuple Prompt env

            Right (Tuple _ env') ->
             pure $ Tuple Prompt env'

  in do
    interface <- createInterface
    setPrompt language.textPrompt interface
    setHandler interface language.initialState handler
    prompt interface


startsWith :: String -> String -> Boolean
startsWith prefix s =
  prefix == take (length prefix) s
