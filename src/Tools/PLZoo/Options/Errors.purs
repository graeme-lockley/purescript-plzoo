module Tools.PLZoo.Options.Errors
  ( Error(..)
  ) where

import Prelude (class Show, (<>), show)

import Text.Parsing.Parser.Pos (Position(..))


data Error
  = SyntaxError { message :: String, position :: Position }
  | UnknownOptionName String
  | ExpectedSwitchOption String
  | InvalidSwitchOption String String
  | ExpectedIDOption String

instance showError :: Show (Error) where
  show (SyntaxError { message: message, position: position }) =
    showPosition position <> ": " <> message
  show (UnknownOptionName n) = "Error: Unknown option " <> n <> "."
  show (ExpectedSwitchOption n) = "Error: The option " <> n <> " is a switch but is being provided with a parameter."
  show (InvalidSwitchOption n v) = "Error: The value " <> v <> " is not valid for " <> n <> "."
  show (ExpectedIDOption n) = "Error: the option " <> n <> " expects ID values."

showPosition :: Position -> String
showPosition (Position { line: line, column: column }) =
   "(" <> (show column) <> ", " <> (show line) <> ")"

