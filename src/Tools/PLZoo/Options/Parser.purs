module Tools.PLZoo.Options.Parser
  ( Value(..)
  , parse
  , parseOption
  ) where

import Prelude (class Eq, class Show, bind, pure, ($), (<>), (<*))
import Control.Alt ((<|>))
import Data.Either (Either(..))
import Text.Parsing.Parser (ParseError(..), Parser, runParser)
import Text.Parsing.Parser.Combinators (try)
import Text.Parsing.Parser.String (eof)

import Tools.PLZoo.Options.Errors (Error(..))
import Tools.PLZoo.Options.Lexer


data Value
  = VSwitch String
  | VStringOption String String
  | VIDOption String String

instance showValue :: Show (Value) where
  show (VSwitch s) = ":" <> s
  show (VStringOption o v) = ":" <> o <> " " <> v
  show (VIDOption o v) = ":" <> o <> " " <> v

derive instance eqValue :: Eq (Value)


parse :: String -> Either Error Value
parse text =
  case runParser text parseOptions of
    Left (ParseError message position) ->
      Left $ SyntaxError { message, position }

    Right e ->
      Right e


parseOptions :: Parser String Value
parseOptions =
  parseOption <* eof


parseOption :: Parser String Value
parseOption =
  try parseIDOption <|> try parseStringOption <|> parseSwitchOption


parseIDOption :: Parser String Value
parseIDOption = do
  _ <- symbol ":"
  name <- id
  option <- id
  pure $ VIDOption name option


parseStringOption :: Parser String Value
parseStringOption = do
  _ <- symbol ":"
  name <- id
  value <- constantString
  pure $ VStringOption name value


parseSwitchOption :: Parser String Value
parseSwitchOption = do
  _ <- symbol ":"
  name <- id
  pure $ VSwitch name


