module Tools.PLZoo.Options.Lexer
  ( constantString
  , id
  , symbol
  ) where

import Prelude
import Data.Array ((:), many, toUnfoldable)
import Data.Char.Unicode (isDigit, isLetter)
import Data.String.CodeUnits (fromCharArray)
import Text.Parsing.Parser (Parser)
import Text.Parsing.Parser.String (string, satisfy, whiteSpace)


lexeme :: forall a. Parser String a -> Parser String a
lexeme p =
  p <* whiteSpace


symbol :: String -> Parser String String
symbol s =
  lexeme $ string s


id :: Parser String String
id = do
  first <- satisfy isLetter
  rest <- lexeme $ many $ satisfy (\c -> isLetter c || isDigit c)
  pure $ fromCharArray <<< toUnfoldable $ first : rest


satisfyMany :: (Char -> Boolean) -> Parser String String
satisfyMany f = do
  match <- many $ satisfy f
  pure $ fromCharArray <<< toUnfoldable $ match


constantString :: Parser String String
constantString =
  symbol "\"" *> satisfyMany (\c -> c /= '"') <* symbol "\""
