module Tools.PLZoo.Options
  ( Command(..)
  , CommandResult(..)
  , Options
  , Setting(..)
  , execute
  ) where

import Prelude

import Control.Monad.State.Trans as StateT
import Data.Array (find)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)

import Tools.PLZoo.Options.Errors (Error(..))
import Tools.PLZoo.Options.Parser (Value(..), parse)


type Options a =
  Array (Command a)


data Command a =
  Command String (Setting a)


data CommandResult
  = Quit
  | Continue
  | ProcessContent String

derive instance eqCommandResult :: Eq (CommandResult)


data Setting a
  = Switch (StateT.StateT a Effect CommandResult)
  | Argument (String -> StateT.StateT a Effect CommandResult)
  | Options (Array (Tuple String (StateT.StateT a Effect CommandResult)))


process :: forall a. Options a -> Value -> StateT.StateT a Effect (Either Error CommandResult)
process options v =
  let
    name =
      case v of
        VSwitch n -> n
        VStringOption n _ -> n
        VIDOption n _ -> n

    option =
      find (\(Command n s) -> n == name) options

    switchOption :: forall b. String -> Array (Tuple String b) -> Maybe (Tuple String b)
    switchOption n opts =
      find (\(Tuple n' _) -> n == n') opts
  in
    case option of
      Nothing ->
        pure $ Left $ UnknownOptionName name

      Just (Command _ (Switch e))  ->
        case v of
          VSwitch _ -> do
            result <- e
            pure $ Right result

          _ ->
            pure $ Left $ ExpectedSwitchOption name

      Just (Command _ (Argument cmd)) ->
        case v of
          VIDOption _ idValue -> do
            result <- cmd $ idValue <> ".utl"
            pure $ Right result

          VSwitch _ -> do
            result <- cmd ""
            pure $ Right result

          VStringOption _ stringValue -> do
            result <- cmd stringValue
            pure $ Right result

      Just (Command _ (Options o)) ->
        case v of
          VIDOption _ idValue ->
            case switchOption idValue o of
              Nothing ->
                pure $ Left $ InvalidSwitchOption name idValue

              Just (Tuple _ cmd) -> do
                result <- cmd
                pure $ Right result

          _ ->
            pure $ Left $ ExpectedIDOption name


execute :: forall a. Options a -> String -> StateT.StateT a Effect (Either Error CommandResult)
execute options s =
  case parse s of
    Left e ->
      pure $ Left e

    Right v ->
      process options v
