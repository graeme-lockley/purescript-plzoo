# purescript-plzoo

A PureScript version of the programming languages zoo.

## Calc

An integer expression calculator.

References:

* [Programming Languages Zoo](http://plzoo.andrej.com)
* [Coding a simple calculator in Purescript](https://thimoteus.github.io/posts/2016-05-16-calculator.html)
* [Into to Parsing with Parsec in Haskell](https://jakewheat.github.io/intro_to_parsing/)

